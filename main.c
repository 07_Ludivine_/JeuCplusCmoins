/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Ludivine
 *
 * Created on 28 février 2018, 18:07
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
 * 
 */
const int MIN = 0;
int max = 0;
int niveau = 0;
int modeJeu = 0;
int essai = 0;
int nombreMystere=0;
int i=1;
int newPartie=1;

void choixNiveau() {
    printf("Choisissez un niveau:\n");
    printf("Pour le niveau 1 tapez 1\n");
    printf("Pour le niveau 2 tapez 2\n");
    printf("Pour le niveau 3 tapez 3\n");
    scanf("%d", &niveau);
    switch (niveau) {
        case 1:
            printf("Vous avez pris le niveau 1\n");
            max = 100;
            break;
        case 2:
            printf("Vous avez pris le niveau 2\n");
            max = 1000;
            break;
        case 3:
            printf("Vous avez pris le niveau 3\n");
            max = 10000;
            break;
        default:
            printf("Veuillez choisir un niveau !\n");
            choixNiveau();
            break;
    }

}

void choixNombreMystere(){
    do {
        printf("Joueur 1 Veuillez entrer un nombre entre %d et %d \n", MIN, max);
        scanf("%d", &nombreMystere);
    } while (nombreMystere > max || nombreMystere < MIN);
}

void choixModeDeJeu() {
    printf("Choisissez un mode de jeu:\n");
    printf("Pour une partie contre l'ordinateur tapez 1\n");
    printf("Pour jouer contre une autre personne tapez 2\n");
    scanf("%d", &modeJeu);
    if (modeJeu==1){
            printf("Vous jouez contre l'ordinateur\n");
            srand(time(NULL));
            nombreMystere = (rand() % (max - MIN + 1)) + MIN;
    }
    else if(modeJeu==2){
            printf("Vous jouez contre une autre personne\n");
            choixNombreMystere();
            printf("Joueur 2\n");
    }
    else{
            printf("Veuillez choisir un mode de jeu!\n");
            choixModeDeJeu();
    }
}
void jouer(){
    printf("Veuillez entrer un nombre entre %d et %d \n", MIN, max);
    do{        
        scanf("%d", &essai);
        if(essai>max || essai<MIN){
            printf("Veuillez entrer un nombre entre %d et %d \n", MIN, max);
        }else{
            if(essai>nombreMystere){
               printf("c'est moins!\n"); 
            }
            else if(essai<nombreMystere){
                printf("c'est plus!\n");
            }
            else {
                printf("Bravo vous avez trouvé en %d essai \n", i );
            };
        } 
        i++;
    }while(essai!=nombreMystere);
}
void nouvellePartie(){
    printf("Pour rejouer tapez 1\n");
    printf("Pour arreter tapez une autre touche\n");
    scanf("%d", &newPartie);
    if (newPartie!=1){
        newPartie=0;
    }
}

int main(int argc, char** argv) {
    while(newPartie=1){
    printf("Bonjour\n");
    choixNiveau();
    choixModeDeJeu();
    jouer();
    nouvellePartie();
    }
    return (EXIT_SUCCESS);
}

